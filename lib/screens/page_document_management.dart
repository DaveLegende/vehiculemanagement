import 'package:confirmation/constantes/constants.dart';
import 'package:confirmation/models/list_document_management.dart';
import 'package:flutter/material.dart';
import 'dart:io';
import 'package:image_picker/image_picker.dart';

class DocumentManagement extends StatefulWidget {
  @override
  _DocumentManagementState createState() => _DocumentManagementState();
}

class _DocumentManagementState extends State<DocumentManagement> {
  _openIdentiteGalerie(BuildContext context) async {
    var picture = await ImagePicker.pickImage(source: ImageSource.gallery);

    this.setState(() {
      _image_id = picture;
    });
    Navigator.of(context).pop();
  }

  _openDrivingGalerie(BuildContext context) async {
    var picture = await ImagePicker.pickImage(source: ImageSource.gallery);

    this.setState(() {
      _image_vic = picture;
    });
    Navigator.of(context).pop();
  }

  _openIdentiteCamera(BuildContext context) async {
    var picture = await ImagePicker.pickImage(source: ImageSource.camera);

    this.setState(() {
      _image_id = picture;
    });
    Navigator.of(context).pop();
  }

  _openDrivingCamera(BuildContext context) async {
    var picture = await ImagePicker.pickImage(source: ImageSource.camera);

    this.setState(() {
      _image_vic = picture;
    });
    Navigator.of(context).pop();
  }

  Future<void> _showChoiceIdentiteDialog(BuildContext context) async {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Photo de profil"),
            content: SingleChildScrollView(
              child: ListBody(children: [
                GestureDetector(
                    child: Text("Galerie",
                        style: TextStyle(color: Colors.grey[900])),
                    onTap: () {
                      _openIdentiteGalerie(context);
                    }),
                Padding(
                  padding: EdgeInsets.all(8.0),
                ),
                GestureDetector(
                    child: Text("Camera",
                        style: TextStyle(color: Colors.grey[900])),
                    onTap: () {
                      _openIdentiteCamera(context);
                    }),
              ]),
            ),
          );
        });
  }

  Future<void> _showChoiceDrivingDialog(BuildContext context) async {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Photo de profil"),
            content: SingleChildScrollView(
              child: ListBody(children: [
                GestureDetector(
                    child: Text("Galerie",
                        style: TextStyle(color: Colors.grey[900])),
                    onTap: () {
                      _openDrivingGalerie(context);
                    }),
                Padding(
                  padding: EdgeInsets.all(8.0),
                ),
                GestureDetector(
                    child: Text("Camera",
                        style: TextStyle(color: Colors.grey[900])),
                    onTap: () {
                      _openDrivingCamera(context);
                    }),
              ]),
            ),
          );
        });
  }

  File _image_id;
  File _image_vic;

  Future<Null> getImageIdentiteByCamera() async {
    final image = await ImagePicker.pickImage(source: ImageSource.camera);

    this.setState(() {
      _image_id = image;
    });
  }

  Future<Null> getImageDrivingByCamera() async {
    final image = await ImagePicker.pickImage(source: ImageSource.camera);

    this.setState(() {
      _image_vic = image;
    });
  }

  List<String> images = ["assets/contact1.png", "assets/contact2.png"];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        title:
            Text("Document Management", style: TextStyle(color: Colors.white)),
        centerTitle: true,
        elevation: 10.0,
        backgroundColor: mBotomColor,
        iconTheme: IconThemeData(color: Colors.yellow[400]),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Container(
              height: 200.0,
              child: Card(
                elevation: 2.0,
                child: Container(
                  padding: EdgeInsets.all(20.0),
                  child: InkWell(
                    onTap: () {
                      _showChoiceIdentiteDialog(context);
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          child: _image_id == null
                              ? pickPhotoCarteIdentite()
                              : pickPhotoCarteIdentiteChoice(),
                        ),
                        Align(
                            alignment: Alignment.bottomLeft,
                            child: Text("Identification cards",
                                style: TextStyle(
                                    color: Colors.grey[900], fontSize: 20.0))),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            Container(
              height: 200.0,
              child: Card(
                elevation: 2.0,
                child: Container(
                  padding: EdgeInsets.all(20.0),
                  child: InkWell(
                    onTap: () {
                      _showChoiceDrivingDialog(context);
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        _image_vic == null
                            ? pickPhotoDriving()
                            : pickPhotoDrivingChoice(),
                        Align(
                            alignment: Alignment.bottomLeft,
                            child: Text("Driving license",
                                style: TextStyle(
                                    color: Colors.grey[900], fontSize: 20.0))),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget pickPhotoCarteIdentiteChoice() {
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: Container(
        // height: MediaQuery.of(context).size.height,
        // width: MediaQuery.of(context).size.width,
        child: Container(
            child: Image.file(
          _image_id,
          fit: BoxFit.cover,
        )),
      ),
    );
  }

  Widget pickPhotoDrivingChoice() {
    return ClipRect(
      child: Image.file(
        _image_vic,
        height: 180.0,
      ),
    );
  }

  Row pickPhotoCarteIdentite() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
          height: 120.0,
          child: Container(
              height: 75.0,
              width: 100,
              child: Image.asset(
                "${images[0]}",
                fit: BoxFit.cover,
              )),
        ),
        Container(
          child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
            Container(height: 40.0, width: 150.0, color: Colors.grey[400]),
            padding(),
            Container(height: 10.0, width: 150.0, color: Colors.grey[400]),
            padding(),
            Container(height: 10.0, width: 150.0, color: Colors.grey[400]),
            padding(),
            Container(height: 10.0, width: 150.0, color: Colors.grey[400]),
          ]),
        ),
      ],
    );
  }

  Row pickPhotoDriving() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
          height: 120.0,
          child: Container(
              height: 75.0,
              width: 100,
              child: Image.asset(
                "${images[1]}",
                fit: BoxFit.cover,
              )),
        ),
        Container(
          child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
            Container(height: 40.0, width: 150.0, color: Colors.grey[400]),
            padding(),
            Container(height: 10.0, width: 150.0, color: Colors.grey[400]),
            padding(),
            Container(height: 10.0, width: 150.0, color: Colors.grey[400]),
            padding(),
            Container(height: 10.0, width: 150.0, color: Colors.grey[400]),
          ]),
        ),
      ],
    );
  }

  Padding padding() {
    return Padding(
      padding: EdgeInsets.only(bottom: 10.0),
    );
  }
}
