import 'package:confirmation/constantes/constants.dart';
import 'package:confirmation/models/list_caracteristique_vehicule.dart';
import 'package:confirmation/screens/page_document_management.dart';
import 'package:flutter/material.dart';
import 'package:confirmation/models/list_ajout_vehicule.dart';


class AjouterVehicule extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        elevation: 10.0,
        backgroundColor: mBotomColor,
        title: Text("Add a new vehicule", style: TextStyle(color: Colors.white)),
        iconTheme: IconThemeData(
          color: Colors.yellow[400]
        ),
        centerTitle: true,
      ),
      body: RefreshIndicator(
        onRefresh: () {
          Navigator.pushReplacement(context, PageRouteBuilder(
            pageBuilder: (a, b, c) => AjouterVehicule(),
            transitionDuration: Duration(seconds: 0),
          ));
          return Future.value(false);
        },
        child: Center(
          child: ListView.builder(
            itemCount: ajouterVehicule.length,
            itemBuilder: (context, index) {
              return Column(
                mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(padding: EdgeInsets.all(10.0), child: Align(alignment: Alignment.topLeft, child: Text("${ajouterVehicule[index]}"))),
                    Container(
                      padding: EdgeInsets.symmetric(
                        horizontal: 7.0,
                      ),
                      child: TextField(
                        decoration: InputDecoration(
                          suffixIcon: IconButton(
                            icon: Icon(Icons.keyboard_arrow_right),
                            onPressed: () {

                            }
                          ),
                          labelText: "${listeCaracteristiqueVoiture[index]}",
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5.0),
                          ),
                        ),
                      ),
                    ),
                    // Container(
                    //   padding: EdgeInsets.all(7.0),
                    //   height: 65.0,
                    //   child: Card(
                    //     elevation: 0,
                    //     child: Row(
                    //       mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    //       children: [
                    //         Container(
                    //           padding: EdgeInsets.only(left: 10.0), 
                    //           child: Text("${listeCaracteristiqueVoiture[index]}", style: TextStyle(color: Colors.black, fontStyle: FontStyle.normal, fontSize: 18.0)),
                    //         ),
                    //         Icon(Icons.keyboard_arrow_right),
                    //       ],
                    //     ),
                    //   ),
                    // ),
                  ]
              );
            },
          ),
        ),
      ),
      bottomNavigationBar: Container(
        margin: EdgeInsets.only(top: 20.0),
        child: InkWell(
          onTap: () {
            Navigator.push(context, MaterialPageRoute(
              builder: (BuildContext buildContext) {
                return DocumentManagement();
              }
            ));
          },
          child: Container(
            width: MediaQuery.of(context).size.width,
            color: mBotomColor,
            height: 50.0,
            child: Center(
              child: Text("COMPLETE", textAlign: TextAlign.center, style: TextStyle(color: Colors.white, fontSize: 20.0)),
            ),
          ),
        ),
      ),
    );
  }

  Padding padding() {
    return Padding(
      padding: EdgeInsets.only(top: 15.0,),
    );
  }
}