import 'package:confirmation/screens/page_ajouter.dart';
import 'package:flutter/material.dart';
import 'package:confirmation/models/list_vehicule.dart';
import 'package:confirmation/constantes/constants.dart';
import 'package:flutter/services.dart';

void main() {

  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp
  ]);

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        
        primarySwatch: Colors.blue,
        
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      debugShowCheckedModeBanner: false,
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {


  @override
  Widget build(BuildContext context) {
    
    Orientation orientation = MediaQuery.of(context).orientation;

    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        elevation: 10.0,
        backgroundColor: mBotomColor,
        title: Text("Vehicule Management", style: TextStyle(color: Colors.white)),
        centerTitle: true,
        leading: Icon(Icons.arrow_back, color: Colors.yellow[500]),
      ),
      body: RefreshIndicator(
        onRefresh: () {
          Navigator.pushReplacement(context, PageRouteBuilder(
            pageBuilder: (a, b, c) => MyApp(),
            transitionDuration: Duration(seconds: 0),
          ));
          return Future.value(false);
        },
        child: Center(
          child: ListView.builder(
            itemCount: listeVehicules.length,
            itemBuilder: (context, index) {
              String cle = listeVehicules[0];
              return Dismissible(
                key: Key(cle),
                child: Container(
                  padding: EdgeInsets.all(5.0),
                  height: 95.0,
                  child: Card(
                    elevation: 5.0,
                    child: Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            padding: EdgeInsets.only(left: 10.0),
                            child: ClipOval(
                              child: Container(
                                color: Colors.yellow[500],
                                height: 50.0,
                                width: 50.0,
                                child: Icon(Icons.description, color: Colors.white),
                              ),
                            ),
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Text("${listeVehicules[index]}", style: TextStyle(color: Colors.grey[900], fontStyle: FontStyle.normal)),
                              Text("43A 235.70", style: TextStyle(color: Colors.grey[400]),),
                            ],
                          ),
                          Container(
                            padding: EdgeInsets.only(right: 10.0),
                            child: ClipOval(
                                child: Container(
                                color: Colors.yellow[400],
                                height: 25.0,
                                width: 25.0,
                                child: Icon(Icons.check, color: Colors.white),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              );
            },
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        elevation: 5.0,
        backgroundColor: Colors.yellow[500],
        onPressed: () {
          setState((){
            Navigator.push(context, MaterialPageRoute(
              builder: (BuildContext buildContext) {
                return AjouterVehicule();
              }
            ));
          });
        },
        tooltip: 'Ajouter',
        child: Icon(Icons.add, size: 30.0, color: Colors.grey[900]),
      ), 
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}
